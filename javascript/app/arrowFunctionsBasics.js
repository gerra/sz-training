//ЗАДАЧА 1

/*УСЛОВИЕ ЗАДАЧИ
Замените код Function Expression стрелочной функцией:

function ask(question, yes, no) {
  if (confirm(question)) yes()
  else no();
}

ask(
  "Вы согласны?",
  function() { alert("Вы согласились."); },
  function() { alert("Вы отменили выполнение."); }
);
*/

//РЕШЕНИЕ
console.log('Задача 1');

let ask = (question, yes, no) => {
	confirm(question)
		? yes()
		: no();
}

 ask('Вы согласны?',
	() => console.log('Вы согласились.'),
	() => console.log('Вы отменили выполнение.')
);
