//ЗАДАЧА 1

/*УСЛОВИЕ ЗАДАЧИ
Напишите код, выполнив задание из каждого пункта отдельной строкой:
Создайте пустой объект user.
Добавьте свойство name со значением John.
Добавьте свойство surname со значением Smith.
Измените значение свойства name на Pete.
Удалите свойство name из объекта.
*/

//РЕШЕНИЕ
console.log('Задача 1');

let user = {
    name: 'John',
    surname: 'Smith'
};

user.name = 'Pete';
delete user.name;

console.log(user.name); // undefined, т к свойство объекта было удалено


//ЗАДАЧА 2

/*УСЛОВИЕ ЗАДАЧИ
Напишите функцию isEmpty(obj), которая возвращает true, если у объекта нет свойств, иначе false.
Должно работать так:
let schedule = {};
alert( isEmpty(schedule) ); // true
schedule["8:30"] = "get up";
alert( isEmpty(schedule) ); // false
*/

//РЕШЕНИЕ
console.log('Задача 2');

let schedule = {};

console.log(isEmpty(schedule)); // true

schedule['8:30'] = 'get up';

console.log(isEmpty(schedule)); // false

function isEmpty(obj) {
    for (let key in obj) {
        return 'false';
    }
    return 'true';
}


//ЗАДАЧА 3

/*УСЛОВИЕ ЗАДАЧИ
У нас есть объект, в котором хранятся зарплаты нашей команды:

let salaries = {
    John: 100,
    Ann: 160,
    Pete: 130
}

Напишите код для суммирования всех зарплат и сохраните результат в переменной sum. Должно получиться 390.
Если объект salaries пуст, то результат должен быть 0.
*/

//РЕШЕНИЕ
console.log('Задача 3');

let salaries = {
    John: 100,
    Ann: 160,
    Pete: 130
};

let empty = {};

function getBudget(obj) {
    let sum = 0;

    for (let key in obj) {
        sum += obj[key];
    }

    return sum;
}

console.log(getBudget(salaries)); // 390
console.log(getBudget(empty)); // 0


//ЗАДАЧА 4

/*УСЛОВИЕ ЗАДАЧИ
Создайте функцию multiplyNumeric(obj), которая умножает все числовые свойства объекта obj на 2.
Например:
// до вызова функции
let menu = {
    width: 200,
    height: 300,
    title: "My menu"
};

multiplyNumeric(menu);

// после вызова функции
menu = {
    width: 400,
    height: 600,
    title: "My menu"
};

Обратите внимание, что multiplyNumeric не нужно ничего возвращать. Следует напрямую изменять объект.
P.S. Используйте typeof для проверки, что значение свойства числовое.
*/

//РЕШЕНИЕ
console.log('Задача 4');

let menu = {
    width: 200,
    height: 300,
    title: "My menu"
};

function multiplyNumeric(obj) {
    for (let key in obj) {
        typeof obj[key] === 'number'
            ? obj[key] *= 2
            : obj[key];
    }
}

multiplyNumeric(menu);
console.log(menu);//выводит измененные числовые значения свойств объекта (x2)
