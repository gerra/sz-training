﻿//ЗАДАЧА 1

/*УСЛОВИЕ ЗАДАЧИ
Напишите условие if для проверки, что переменная age находится в диапазоне между 14 и 90 включительно.
«Включительно» означает, что значение переменной age может быть равно 14 или 90.
*/

//РЕШЕНИЕ
console.log('Задача 1');

let age = +prompt('Проверка значения переменной age (диапазон от 14 до 90)', '');

if (age >= 14 && age <= 90) {
    console.log('Значение в диапазоне');
} else {
    console.log('Значение ВНЕ диапазона');
}


//ЗАДАЧА 2

/*УСЛОВИЕ ЗАДАЧИ
Напишите условие if для проверки, что значение переменной age НЕ находится в диапазоне 14 и 90 включительно.
Напишите два варианта: первый с использованием оператора НЕ !, второй – без этого оператора.
*/

//РЕШЕНИЕ
console.log('Задача 2');

let age = +prompt('Проверка значения переменной age (диапазон от 14 до 90)', '');

if (!(age >= 14 && age <= 90)) {
    console.log('Значение в диапазоне');
} else {
    console.log('Значение ВНЕ диапазона');
}

let age = +prompt('Проверка значения переменной age (диапазон от 14 до 90)', '');

if (age < 14 || age > 90) {
    console.log('Значение в диапазоне');
} else {
    console.log('Значение ВНЕ диапазона');
}


//ЗАДАЧА 3

/*УСЛОВИЕ ЗАДАЧИ
Напишите код, который будет спрашивать логин с помощью prompt.
Если посетитель вводит «Админ», то prompt запрашивает пароль,
если ничего не введено или нажата клавиша Esc – показать «Отменено»,
в противном случае отобразить «Я вас не знаю».
Пароль проверять так:
Если введён пароль «Я главный», то выводить «Здравствуйте!»,
Иначе – «Неверный пароль»,
При отмене – «Отменено».
*/

//РЕШЕНИЕ
console.log('Задача 3');

let login = prompt('Введите логин', '');

if (login === 'Админ') {

    let password = prompt('Введите пароль', '');

    if (password === 'Я главный') {
        console.log('Здравствуйте!');
    } else if (!password) {
        console.log('Отменено');
    } else {
        console.log('Неверный пароль');
    }

} else if (!login) {
    console.log('Отменено');
} else {
    console.log('Я вас не знаю');
}
